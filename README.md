Design Challenge
====================

This design challenge allows you to choose your own path and lets you flaunt your creative panache and technical skills along the way. Get as much completed as you can, with the focus great UI/UX.

In a fictional world, we are branching into the music business and need a new a website. This website needs to display the top 100 songs based on the iTunes API.   

#### Base Requirements For All Levels
-------
- Basic mockups demonstrating (Adobe Photoshop, InVIsion or Adobe XD) :
  - A clean modern look (Use of UI/UX good practices)
  - A good user experience (use of iconagraphy for example FontAwesome or Marial Icon)
  


#### Levels of Awesome

Choose one of the following routes for your journey. 

-------
### Novice

*"Hey! Look! Listen!"*

**TASKS**
* All of the base requirements
+ Designs for mobile, tablet, and desktop

-------
### Intermediate

*"I know Kung Fu."*

**TASKS**
* All of the base, and novice requirements
+ High fidelity mockups


-------
### Expert

*"Watch and learn Grasshopper."*

**TASKS**
* All of the base, novice, and intermediate requirements
+ Prototype your designs using a frontend technology of your choice

-------
### Bonus Round

*"All is fair in love and bonus rounds"*

**TASKS**
+ Add an awesome feature to make the site even better
+ Add real data using the top 100 albums based on the [JSON]('http://www.w3schools.com/json/') feed here:  https://itunes.apple.com/us/rss/topalbums/limit=100/json
+ For an even greater challenge, implement the M2Gen [FrontendChallenge](https://bitbucket.org/m2gen/frontendchallenge)